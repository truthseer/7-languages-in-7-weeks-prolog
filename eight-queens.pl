valid_queen((Row, Col)) :-
    Range = [1, 2, 3, 4, 5, 6, 7, 8],
    member(Row, Range), member(Col, Range).

valid_board([]).
valid_board([Head|Rest]) :-
    valid_queen(Head),
    valid_board(Rest).

rows([], []).
rows([(Row, _)|QueensRest], [Row|RowsRest]) :-
    rows(QueensRest, RowsRest).

cols([], []).
cols([(_, Col)|QueensRest], [Col|ColsRest]) :-
    cols(QueensRest, ColsRest).

diags1([], []).
diags1([(Row, Col)|QueensRest], [Diagonal|DiagonalsRest]) :-
    Diagonal is Col - Row,
    diags1(QueensRest, DiagonalsRest).

diags2([], []).
diags2([(Row, Col)|QueensRest], [Diagonal|DiagonalsRest]) :-
    Diagonal is Col + Row,
    diags2(QueensRest, DiagonalsRest).

eight_queens(Board) :-
    length(Board, 8),
    valid_board(Board),

    rows(Board, Rows),
    cols(Board, Cols),
    diags1(Board, Diags1),
    diags1(Board, Diags2),

    fd_all_different(Rows),
    fd_all_different(Cols),
    fd_all_different(Diags1),
    fd_all_different(Diags2).
