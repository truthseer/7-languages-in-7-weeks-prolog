sort_list([], []).
sort_list(List, NewList) :-
    allSmallest(List, [Head|Rest]),
    remove(Head, List, List1),
    sort_list(List1, Sorted),
    append([Head|Rest], Sorted, NewList), !.

remove(A, [], []).
remove(A, [Head|Rest], NewList) :-
    A=Head,
    remove(A, Rest, NewList).
remove(A, [Head|Rest], NewList) :-
    remove(A, Rest, List),
    append([Head], List, NewList).

allSmallest([E], [E]).
allSmallest([Head|Rest], Smallest) :-
    allSmallest(Rest, [Head1|Rest1]),
    smallest(Head, Head1, [Head], [Head1|Rest1], Smallest).

smallest(A, B, Choice1, Choice2, Which) :-
    B<A,
    append(Choice2, [], Which).
smallest(A, B, Choice1, Choice2, Which) :-
    A<B,
    append(Choice1, [], Which).
smallest(A, B, Choice1, Choice2, Which) :- append(Choice1, Choice2, Which).
