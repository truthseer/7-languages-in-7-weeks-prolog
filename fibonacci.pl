fibonacci(N, F) :- N>0, N1 is N-1, fibonacci(N1, 0, 0, 1, F).

fibonacci(N, N, CA, NA, CA).
fibonacci(N, T, CA, NA, F) :-
    N>T,
    T1 is T+1,
    CA1 is NA,
    NA1 is CA+NA,
    fibonacci(N, T1, CA1, NA1, F).
