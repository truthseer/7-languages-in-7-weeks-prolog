valid_queen((_, Col)) :- member(Col, [1, 2, 3, 4, 5, 6, 7, 8]).

valid_board([]).
valid_board([Head|Rest]) :-
    valid_queen(Head),
    valid_board(Rest).

cols([], []).
cols([(_, Col)|QueensRest], [Col|ColsRest]) :-
    cols(QueensRest, ColsRest).

diags1([], []).
diags1([(Row, Col)|QueensRest], [Diagonal|DiagonalsRest]) :-
    Diagonal is Col - Row,
    diags1(QueensRest, DiagonalsRest).

diags2([], []).
diags2([(Row, Col)|QueensRest], [Diagonal|DiagonalsRest]) :-
    Diagonal is Col + Row,
    diags2(QueensRest, DiagonalsRest).

eight_queens(Board) :-
    Board = [(1, _), (2, _), (3, _), (4, _), (5, _), (6, _), (7, _), (8, _)],
    valid_board(Board),

    cols(Board, Cols),
    diags1(Board, Diags1),
    diags1(Board, Diags2),

    fd_all_different(Cols),
    fd_all_different(Diags1),
    fd_all_different(Diags2).
