queens(Queens) :-
    valid_board(Queens),
    check_rows(Queens),
    check_cols(Queens),
    check_diags(Queens).

valid_board(Board) :- valid_board(Board, 0).
valid_board([], 8).
valid_board([Head|Rest], Row) :-
    member(Head, [1, 2, 3, 4, 5, 6, 7, 8]),
    Row1 is Row + 1,
    valid_board(Rest, Row1).

check_rows(Queens) :- length(Queens, 8).

check_cols(Queens) :-
    fd_domain(Queens, 1, 8),
    fd_all_different(Queens).

check_diags(Queens) :-
    diags(Queens, Diags1, Diags2),
    fd_all_different(Diags1),
    fd_all_different(Diags2).

diags(Queens, Diags1, Diags2) :- diags(Queens, 0, Diags1, Diags2).
diags([], 8, [], []).
diags([Head|Rest], Row, [Diagonal1|Diags1], [Diagonal2|Diags2]) :-
    Diagonal1 is Head - Row,
    Diagonal2 is Head + Row,
    Row1 is Row + 1,
    diags(Rest, Row1, Diags1, Diags2).
