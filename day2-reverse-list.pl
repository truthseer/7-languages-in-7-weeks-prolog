reverse_list([], []).
reverse_list([Head|Rest], NewList) :-
    reverse_list(Rest, OtherList),
    append(OtherList, [Head], NewList).
