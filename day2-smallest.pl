smallest([E], E).
smallest([Head|Rest], What) :-
    smallest(Rest, Current),
    smallest(Head, Current, What), !.

smallest(A, B, What) :-
    A<B,
    What is A.
smallest(A, B, B).
