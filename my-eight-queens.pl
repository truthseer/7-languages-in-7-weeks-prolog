eight_queens(List) :-
    length(List, 8),
    valid_board(List),
    check_rows_cols(List),
    check_diagonals(List).

valid_board([]).
valid_board([Head|Rest]) :-
    valid_queen(Head),
    valid_board(Rest).

valid_queen((Row, Col)) :-
    Range = [1, 2, 3, 4, 5, 6, 7, 8],
    member(Row, Range), member(Col, Range).

check_rows_cols(List) :- check_rows_cols(List, [], []).
check_rows_cols([], Rows, Cols) :-
    fd_all_different(Rows),
    fd_all_different(Cols).
check_rows_cols([(First, Second)|Rest], Rows, Cols) :-
    append(Rows, [First], Rows1),
    append(Cols, [Second], Cols1),
    check_rows_cols(Rest, Rows1, Cols1).

check_diagonals(List) :- check_diagonals(List, [], []).
check_diagonals([], Diagonals1, Diagonals2) :-
    fd_all_different(Diagonals1),
    fd_all_different(Diagonals2).
check_diagonals([(Row, Col)|Rest], Diagonals1, Diagonals2) :-
    Diagonal1 is Col + Row,
    Diagonal2 is Col - Row,
    append(Diagonals1, Diagonal1, Diagonals11),
    append(Diagonals2, Diagonal2, Diagonals21),
    check_diagonals(Rest, Diagonals11, Diagonals21).
