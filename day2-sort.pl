sort_list([], []).
sort_list([Head|Rest], NewList) :-
    sort_list(Rest, Sorted),
    placeInOrder(Head, Sorted, NewList), !.

placeInOrder(Number, [], [Number]).
placeInOrder(Number, [Head|Rest], NewList) :-
    Head<Number,
    placeInOrder(Number, Rest, Ordered),
    append([Head], Ordered, NewList).
placeInOrder(Number, [Head|Rest], [Number, Head|Rest]).
