valid([]).
valid([Head|Rest]) :-
    fd_all_different(Head),
    valid(Rest).

sudoku(Puzzle, Solution) :-
    notrace,
    Solution = Puzzle,

    length(Puzzle, 81),
    fd_domain(Puzzle, 1, 9),

    rows(Puzzle, Rows),
    cols(Puzzle, Cols),
    squares(Puzzle, Squares),

    nl, write(Rows), nl, nl,
    write(Cols), nl, nl,
    write(Squares), nl, nl,

    append([], Rows, Blocks1),
    append(Blocks1, Cols, Blocks2),
    append(Blocks2, Squares, Blocks),
    valid(Blocks),
    notrace.

rows([], []).
rows([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest],
        [[E1, E2, E3, E4, E5, E6, E7, E8, E9]|Rows]) :-
    rows(Rest, Rows).

cols(Puzzle, [L1, L2, L3, L4, L5, L6, L7, L8, L9]) :-
    cols(Puzzle, L1, L2, L3, L4, L5, L6, L7, L8, L9).
cols([], [], [], [], [], [], [], [], [], []).
cols([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest],
        [E1|L1], [E2|L2], [E3|L3], [E4|L4], [E5|L5], [E6|L6], [E7|L7], [E8|L8], [E9|L9]) :-
    cols(Rest, L1, L2, L3, L4, L5, L6, L7, L8, L9).

tracing_it(Puzzle) :- length(Puzzle, L), L = 9, trace, !.
tracing_it(_) :- !.

squares(Puzzle, [L1, L2, L3, L4, L5, L6, L7, L8, L9]) :-
    squares(Puzzle, 0, L1, L2, L3, L4, L5, L6, L7, L8, L9).
squares([], Count, [], [], [], [], [], [], [], [], [], []) :- !.
squares([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest], Count,
        [E1|[E2|[E3|S1]]], [E4|[E5|[E6|S2]]], [E7|[E8|[E9|S3]]], S4, S5, S6, S7, S8, S9) :-
    Count < 3,
    Count1 is Count + 1,
    squares(Rest, Count1, S1, S2, S3, S4, S5, S6, S7, S8, S9).
squares([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest], Count,
        S1, S2, S3, [E1|[E2|[E3|S4]]], [E4|[E5|[E6|S5]]], [E7|[E8|[E9|S6]]], S7, S8, S9) :-
    Count < 6,
    Count1 is Count + 1,
    squares(Rest, Count1, S1, S2, S3, S4, S5, S6, S7, S8, S9).
squares([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest], Count,
        S1, S2, S3, S4, S5, S6, [E1|[E2|[E3|S7]]], [E4|[E5|[E6|S8]]], [E7|[E8|[E9|S9]]]) :-
    Count < 9,
    Count1 is Count + 1,
    tracing_it(Rest),
    squares(Rest, Count1, S1, S2, S3, S4, S5, S6, S7, S8, S9).

my_print([E]) :- write(E), nl.
my_print([Head|Rest]) :- write(Head), write(' '), my_print(Rest), !.

pretty_sudoku(Puzzle) :-
    sudoku(Puzzle, Solution),
    nl, print_sudoku(Solution, 0), !.

print_sudoku([], Count) :- my_print(['-------------------------']).
print_sudoku([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest], Count) :-
    Mod is Count mod 3,
    Mod = 0,
    my_print(['-------------------------']),
    my_print(['|', E1, E2, E3, '|', E4, E5, E6, '|', E7, E8, E9, '|']),
    Count1 is Count + 1,
    print_sudoku(Rest, Count1).
print_sudoku([E1, E2, E3, E4, E5, E6, E7, E8, E9|Rest], Count) :-
    my_print(['|', E1, E2, E3, '|', E4, E5, E6, '|', E7, E8, E9, '|']),
    Count1 is Count + 1,
    print_sudoku(Rest, Count1).
